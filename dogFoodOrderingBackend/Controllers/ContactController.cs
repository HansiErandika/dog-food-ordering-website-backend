﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dogFoodOrderingBackend.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace dogFoodOrderingBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        private readonly FoodOrderContext _context;

        public ContactController(FoodOrderContext context)
        {
            _context = context;
        }

        [HttpPost]
        public async Task<IActionResult> AddContact([FromBody] Contact contact)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Contacts.Add(contact);
            await _context.SaveChangesAsync();

            return NoContent();
        }

    }
}
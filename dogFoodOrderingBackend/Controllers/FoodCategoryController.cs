﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using dogFoodOrderingBackend.Models;

namespace dogFoodOrderingBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FoodCategoryController : ControllerBase
    {
        private readonly FoodOrderContext _context;

        public FoodCategoryController(FoodOrderContext context)
        {
            _context = context;
        }

        // GET: api/FoodCategory
        [HttpGet]
        public IEnumerable<FoodCategory> GetFoodCategories()
        {
            return _context.FoodCategories;
        }

        // GET: api/FoodCategory/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFoodCategory([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var foodCategory = await _context.FoodCategories.FindAsync(id);

            if (foodCategory == null)
            {
                return NotFound();
            }

            return Ok(foodCategory);
        }

        // PUT: api/FoodCategory/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFoodCategory([FromRoute] int id, [FromBody] FoodCategory foodCategory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != foodCategory.CId)
            {
                return BadRequest();
            }

            _context.Entry(foodCategory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FoodCategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/FoodCategory
        [HttpPost]
        public async Task<IActionResult> PostFoodCategory([FromBody] FoodCategory foodCategory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.FoodCategories.Add(foodCategory);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFoodCategory", new { id = foodCategory.CId }, foodCategory);
        }

        // DELETE: api/FoodCategory/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFoodCategory([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var foodCategory = await _context.FoodCategories.FindAsync(id);
            if (foodCategory == null)
            {
                return NotFound();
            }

            _context.FoodCategories.Remove(foodCategory);
            await _context.SaveChangesAsync();

            return Ok(foodCategory);
        }

        private bool FoodCategoryExists(int id)
        {
            return _context.FoodCategories.Any(e => e.CId == id);
        }
    }
}
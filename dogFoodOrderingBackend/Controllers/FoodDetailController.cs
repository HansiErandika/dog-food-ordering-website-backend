﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dogFoodOrderingBackend.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace dogFoodOrderingBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FoodDetailController : ControllerBase
    {
        private readonly FoodOrderContext _context;

        public FoodDetailController(FoodOrderContext context)
        {
            _context = context;
        }

        // GET: api/FoodDetail
        [HttpGet]
        public IEnumerable<FoodDetail> Get()
        {
            return _context.FoodDetails;
        }

        // GET: api/FoodDetail/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFoodDetail([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var foodDetail = await _context.FoodDetails.FindAsync(id);

            if (foodDetail == null)
            {
                return NotFound();
            }

            return Ok(foodDetail);
        }
    }
}
﻿using dogFoodOrderingBackend.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;

namespace dogFoodOrderingBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly FoodOrderContext _context;
        private readonly IConfiguration _config;
        private readonly IHostingEnvironment webHostEnvironment;
        private IHttpContextAccessor _httpContextAccessor;
        private string baseURl;

        public LoginController(FoodOrderContext context, IConfiguration config, IHostingEnvironment webHostEnvironment, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _config = config;
            this.webHostEnvironment = webHostEnvironment;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet("users")]
        public IActionResult GetUsers()
        {
            var users = _context.Users.AsQueryable();
            return Ok(users);
        }

        [HttpGet("user/{id}")]
        public async Task<IActionResult> GetUser([FromRoute] int id)
        {
            
            var user = await _context.Users.FindAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            if (user.Image != null)
            {
                baseURl = _httpContextAccessor.HttpContext.Request.Scheme + "://" +
                    _httpContextAccessor.HttpContext.Request.Host +
                    _httpContextAccessor.HttpContext.Request.PathBase;
                user.Image = Path.Combine(baseURl, "Images/Profile", user.Image);
            }
            
            return Ok(user);
        }

        [HttpPost("signup")]
        public IActionResult Signup([FromBody] User userobj)
        {
            if(userobj == null)
            {
                return BadRequest();
            }
            else
            {
                _context.Users.Add(userobj);
                _context.SaveChanges();
                return Ok(new
                {
                    StatusCode = 200,
                    Message = "User Added Successfully"
                }); 
            }
        }
        [Produces("application/json")]
        [HttpPost("imageupload/{id}"), DisableRequestSizeLimit]
        public async Task<IActionResult> ImageUpload([FromRoute] int id)
        {
            try
            {
                var fil = Request.Form.Files[0];
                //var rondom = Guid.NewGuid() + fil.FileName;
                string extension = Path.GetExtension(fil.FileName);
                var imageName = id.ToString() + extension;
                var path = Path.Combine(this.webHostEnvironment.WebRootPath, "Images/Profile", imageName);
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    fil.CopyTo(stream);
                    User userobj = _context.Users.Find(id);
                    userobj.Image = imageName;
                    _context.Entry(userobj).State = EntityState.Modified;
                    _context.SaveChanges();
                }

                var baseURl = _httpContextAccessor.HttpContext.Request.Scheme + "://" +
                    _httpContextAccessor.HttpContext.Request.Host +
                    _httpContextAccessor.HttpContext.Request.PathBase;
                //return Ok("Resources\\Images\\girls_PNG6463.png");
                return Ok(new { 
                    filename=Path.Combine(baseURl, "Images/Profile", imageName),
                    imageName= imageName
                });

 

               
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }
        }


        [HttpPost("uploadFile"),DisableRequestSizeLimit]
        public IActionResult Upload()
        {
            try
            {
                var file = Request.Form.Files[0];

                var provider = new PhysicalFileProvider(webHostEnvironment.WebRootPath);
                var contents = provider.GetDirectoryContents(Path.Combine("uploadedfiles", "images"));

                string wwwPath = this.webHostEnvironment.WebRootPath;
                string contentPath = this.webHostEnvironment.ContentRootPath;

                //string folderName = Path.Combine(this.webHostEnvironment.WebRootPath, "Images");

                var folderName = Path.Combine("Resources", "Images");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                if (file.Length > 0)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var fullPath = Path.Combine(pathToSave, fileName);
                    var dbPath = Path.Combine(folderName, fileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                    //return Ok("Resources\\Images\\girls_PNG6463.png");
                    baseURl = _httpContextAccessor.HttpContext.Request.Scheme + "://" +
                    _httpContextAccessor.HttpContext.Request.Host +
                    _httpContextAccessor.HttpContext.Request.PathBase;
                    //return Ok("Resources\\Images\\girls_PNG6463.png");
                    return Ok(new
                    {
                        filename = Path.Combine(baseURl, folderName, fileName)
                    });
                    //return Ok(new { dbPath });
                    
                }
                else
                {
                    return BadRequest();
                }
            }
            catch(Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }
        }

        [HttpPut("update/{id}")]
        public async Task<IActionResult> Update([FromRoute] int id, [FromBody] User userobj)
        {
            if (userobj == null)
            {
                return BadRequest();
            }
            else
            {
                _context.Entry(userobj).State = EntityState.Modified;
                _context.SaveChanges();
                return Ok(new
                {
                    StatusCode = 200,
                    Message = "User Updated Successfully"
                });
            }
        }

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            var userobj = _context.Users.Find(id);
            if (userobj == null)
            {
                return BadRequest();
            }
            else
            {
                _context.Users.Remove(userobj);
                await _context.SaveChangesAsync();
                return Ok(new
                {
                    StatusCode = 200,
                    Message = "User Delete Successfully"
                });
            }
        }

        [HttpPost("login")]
        public IActionResult Login([FromBody] User userobj)
        {
            if (userobj == null)
            {
                return BadRequest();
            }
            else
            {
                var user = _context.Users.Where(a=>
                a.Email == userobj.Email
                && a.Password == userobj.Password).FirstOrDefault();
                
                if(user != null)
                {
                    var token = GenerateToken(userobj.Email);
                    return Ok(new
                    {
                        StatusCode = 200,
                        Message = "Logged in Successfully",
                        UserData = user.Name,
                        JwtToken=token
                    });
                }
                else
                {
                    return Ok(new {
                        StatusCode = 404,
                        Message = "User Not Found",
                    });
                }
                
            }
        }

        private string GenerateToken(string email)
        {
            var tokenhandler = new JwtSecurityTokenHandler();
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:key"]));
            var credential = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(ClaimTypes.Email,email),
                new Claim("CompanyName","DogFoodOrder")
            };
            var token = new JwtSecurityToken(
                issuer: _config["Jwt:Issuer"],
                audience: _config["Jwt:Audience"],
                claims,
                expires:DateTime.Now.AddDays(1),
                signingCredentials:credential
                );
            return tokenhandler.WriteToken(token);
        }
    }
}

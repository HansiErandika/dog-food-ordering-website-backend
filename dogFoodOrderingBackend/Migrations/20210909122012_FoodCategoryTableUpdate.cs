﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace dogFoodOrderingBackend.Migrations
{
    public partial class FoodCategoryTableUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CatogaryNumber",
                table: "FoodCategories",
                type: "nvarchar(100)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CatogaryNumber",
                table: "FoodCategories");
        }
    }
}

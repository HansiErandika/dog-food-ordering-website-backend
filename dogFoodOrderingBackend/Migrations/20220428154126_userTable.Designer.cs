﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using dogFoodOrderingBackend.Models;

namespace dogFoodOrderingBackend.Migrations
{
    [DbContext(typeof(FoodOrderContext))]
    [Migration("20220428154126_userTable")]
    partial class userTable
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.14-servicing-32113")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("dogFoodOrderingBackend.Models.Contact", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Email");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("Mobile");

                    b.HasKey("Id");

                    b.ToTable("Contacts");
                });

            modelBuilder.Entity("dogFoodOrderingBackend.Models.FoodCategory", b =>
                {
                    b.Property<int>("CId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Catogary")
                        .IsRequired()
                        .HasColumnType("nvarchar(100)");

                    b.Property<string>("CatogaryNumber")
                        .IsRequired()
                        .HasColumnType("nvarchar(100)");

                    b.Property<bool>("IsAvailableCatogary");

                    b.HasKey("CId");

                    b.ToTable("FoodCategories");
                });

            modelBuilder.Entity("dogFoodOrderingBackend.Models.FoodDetail", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("FoodDetails");

                    b.Property<string>("FoodImg");

                    b.Property<string>("FoodName");

                    b.Property<decimal>("Price");

                    b.HasKey("Id");

                    b.ToTable("FoodDetails");
                });
#pragma warning restore 612, 618
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace dogFoodOrderingBackend.Migrations
{
    public partial class updateCategoryTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FoodDetails",
                table: "FoodDetails",
                newName: "Description");

            migrationBuilder.AddColumn<int>(
                name: "CId",
                table: "FoodDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_FoodDetails_CId",
                table: "FoodDetails",
                column: "CId");

            migrationBuilder.AddForeignKey(
                name: "FK_FoodDetails_FoodCategories_CId",
                table: "FoodDetails",
                column: "CId",
                principalTable: "FoodCategories",
                principalColumn: "CId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FoodDetails_FoodCategories_CId",
                table: "FoodDetails");

            migrationBuilder.DropIndex(
                name: "IX_FoodDetails_CId",
                table: "FoodDetails");

            migrationBuilder.DropColumn(
                name: "CId",
                table: "FoodDetails");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "FoodDetails",
                newName: "FoodDetails");
        }
    }
}

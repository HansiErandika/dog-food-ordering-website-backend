﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace dogFoodOrderingBackend.Migrations
{
    public partial class ProductDetailsView : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
create view ProductDetails as
select fd.Id, fd.FoodName, fd.Description, fd.Price, fd.FoodImg, fc.CID, fc.Catogary, fc.CatogaryNumber, fc.IsAvailableCatogary 
from FoodDetails fd inner join FoodCategories fc on fd.CId = fc.CId where fc.IsAvailableCatogary = 1;
");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
drop view ProductDetails;
");

        }
    }
}

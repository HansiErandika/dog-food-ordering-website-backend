﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace dogFoodOrderingBackend.Models
{
    public class FoodCategory
    {
        [Key]
        public int CId { get; set; }
        [Required]
        [Column(TypeName ="nvarchar(100)")]
        public string Catogary { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(100)")]
        public string CatogaryNumber { get; set; }
        [Required]
        public bool IsAvailableCatogary { get; set; }


        public ICollection<FoodDetail> Students { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace dogFoodOrderingBackend.Models
{
    public class FoodDetail
    {
        [Key]
        public int Id { get; set; }

        public string FoodName { get; set; }

        public string Description { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal Price { get; set; }

        public string FoodImg { get; set; }


        //Foreign key for Standard
        public int CId { get; set; }
        public FoodCategory FoodCategory { get; set; }
    }
}

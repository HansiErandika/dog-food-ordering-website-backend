﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dogFoodOrderingBackend.Models
{
    public class FoodOrderContext:DbContext
    {
        public FoodOrderContext(DbContextOptions<FoodOrderContext> options):base(options)
        {

        }

        public DbSet<FoodCategory> FoodCategories { get; set; }

        public DbSet<FoodDetail> FoodDetails { get; set; }

        public DbSet<Contact> Contacts { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<ProductDetail> ProductDetails { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            modelBuilder
          .Entity<ProductDetail>(eb =>
          {
              modelBuilder.Entity<ProductDetail>(entity => {
                  entity.HasKey(e => e.Id);
                  entity.ToTable("ProductDetails");
        
              });
          });
        }



    }
}

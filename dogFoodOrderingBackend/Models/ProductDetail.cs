﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace dogFoodOrderingBackend.Models
{
    public class ProductDetail
    {
        [Key]
        public int Id { get; set; }

        public string FoodName { get; set; }

        public string Description { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal Price { get; set; }

        public string FoodImg { get; set; }

        public int CId { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public string Catogary { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(100)")]
        public string CatogaryNumber { get; set; }

        public bool IsAvailableCatogary { get; set; }
    }
}

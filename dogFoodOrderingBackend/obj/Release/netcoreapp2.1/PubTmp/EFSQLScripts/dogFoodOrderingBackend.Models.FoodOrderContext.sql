﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210909121506_DatabaseCreate')
BEGIN
    CREATE TABLE [FoodCategories] (
        [CId] int NOT NULL IDENTITY,
        [Catogary] nvarchar(100) NOT NULL,
        [IsAvailableCatogary] bit NOT NULL,
        CONSTRAINT [PK_FoodCategories] PRIMARY KEY ([CId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210909121506_DatabaseCreate')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210909121506_DatabaseCreate', N'2.1.14-servicing-32113');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210909122012_FoodCategoryTableUpdate')
BEGIN
    ALTER TABLE [FoodCategories] ADD [CatogaryNumber] nvarchar(100) NOT NULL DEFAULT N'';
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210909122012_FoodCategoryTableUpdate')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210909122012_FoodCategoryTableUpdate', N'2.1.14-servicing-32113');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220428154126_userTable')
BEGIN
    CREATE TABLE [Contacts] (
        [Id] int NOT NULL IDENTITY,
        [FirstName] nvarchar(max) NULL,
        [LastName] nvarchar(max) NULL,
        [Email] nvarchar(max) NULL,
        [Mobile] nvarchar(max) NULL,
        CONSTRAINT [PK_Contacts] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220428154126_userTable')
BEGIN
    CREATE TABLE [FoodDetails] (
        [Id] int NOT NULL IDENTITY,
        [FoodName] nvarchar(max) NULL,
        [FoodDetails] nvarchar(max) NULL,
        [Price] decimal(18, 2) NOT NULL,
        [FoodImg] nvarchar(max) NULL,
        CONSTRAINT [PK_FoodDetails] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220428154126_userTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20220428154126_userTable', N'2.1.14-servicing-32113');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220428154649_userTableEdit')
BEGIN
    DECLARE @var0 sysname;
    SELECT @var0 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[FoodDetails]') AND [c].[name] = N'Price');
    IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [FoodDetails] DROP CONSTRAINT [' + @var0 + '];');
    ALTER TABLE [FoodDetails] ALTER COLUMN [Price] decimal(18,2) NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220428154649_userTableEdit')
BEGIN
    CREATE TABLE [Users] (
        [Id] int NOT NULL IDENTITY,
        [Name] nvarchar(max) NULL,
        [Email] nvarchar(max) NULL,
        [MobileNumber] nvarchar(max) NULL,
        [Password] nvarchar(max) NULL,
        [Status] int NOT NULL,
        CONSTRAINT [PK_Users] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220428154649_userTableEdit')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20220428154649_userTableEdit', N'2.1.14-servicing-32113');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220428155343_updateCategoryTable')
BEGIN
    EXEC sp_rename N'[FoodDetails].[FoodDetails]', N'Description', N'COLUMN';
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220428155343_updateCategoryTable')
BEGIN
    ALTER TABLE [FoodDetails] ADD [CId] int NOT NULL DEFAULT 0;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220428155343_updateCategoryTable')
BEGIN
    CREATE INDEX [IX_FoodDetails_CId] ON [FoodDetails] ([CId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220428155343_updateCategoryTable')
BEGIN
    ALTER TABLE [FoodDetails] ADD CONSTRAINT [FK_FoodDetails_FoodCategories_CId] FOREIGN KEY ([CId]) REFERENCES [FoodCategories] ([CId]) ON DELETE CASCADE;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220428155343_updateCategoryTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20220428155343_updateCategoryTable', N'2.1.14-servicing-32113');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220428161309_ProductDetailsView')
BEGIN

    create view ProductDetails as
    select fd.Id, fd.FoodName, fd.Description, fd.Price, fd.FoodImg, fc.CID, fc.Catogary, fc.CatogaryNumber, fc.IsAvailableCatogary 
    from FoodDetails fd inner join FoodCategories fc on fd.CId = fc.CId where fc.IsAvailableCatogary = 1;

END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220428161309_ProductDetailsView')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20220428161309_ProductDetailsView', N'2.1.14-servicing-32113');
END;

GO

